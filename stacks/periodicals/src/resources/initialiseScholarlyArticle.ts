import { v4 as uuid } from 'uuid';

import {
  NewScholarlyArticle,
  PostInitialiseScholarlyArticleRequest,
  PostInitialiseScholarlyArticleResponse,
} from '../../../../lib/interfaces/endpoints/periodical';
import { Request } from '../../../../lib/lambda/faas';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { SessionContext } from '../../../../lib/lambda/middleware/withSession';
import { convertToOrcidWorkId, isOrcidWorkLink } from '../../../../lib/utils/orcid';
import { initialiseScholarlyArticle as service } from '../services/initialiseScholarlyArticle';
import { fetchPeriodical } from '../services/periodical';
import { fetchScholarlyArticle } from '../services/scholarlyArticle';

export async function initialiseScholarlyArticle(
  context: Request<PostInitialiseScholarlyArticleRequest> & DbContext & SessionContext,
): Promise<PostInitialiseScholarlyArticleResponse> {
  const proposedScholarlyArticle = (context.body && context.body.result)
    ? context.body.result
    : {};
  const periodicalIdentifier = (proposedScholarlyArticle.isPartOf)
    ? proposedScholarlyArticle.isPartOf.identifier
    : undefined;

  if (context.session instanceof Error) {
    throw new Error('You do not appear to be logged in.');
  }

  if (periodicalIdentifier) {
    try {
      await fetchPeriodical(context.database, periodicalIdentifier);
    } catch (e) {
      throw new Error('Could not find the journal to submit to.');
    }
  }

  const fullText = (proposedScholarlyArticle.associatedMedia && proposedScholarlyArticle.associatedMedia.length > 0)
    ? proposedScholarlyArticle.associatedMedia[0]
    : undefined;

  let sameAs;
  if (
    proposedScholarlyArticle.sameAs
    && isOrcidWorkLink(proposedScholarlyArticle.sameAs)
  ) {
    if (context.session.account && context.session.account.orcid) {
      // The linked article is of the form `https://orcid.org/<orcid>/work/<putCode>`.
      // To get the article, we therefore need the 19-character ORCID present after `https://orcid.org`.
      const claimedArticleOrcid = proposedScholarlyArticle.sameAs.substr('https://orcid.org/'.length, 19);
      if (context.session.account.orcid === claimedArticleOrcid) {
        sameAs = proposedScholarlyArticle.sameAs;
      } else {
        throw new Error('You can only claim articles you authored.');
      }
    } else {
      throw new Error('Please sign in with ORCID to claim your articles.');
    }

    // If the article has already been added before, return that
    const knownArticle = await fetchScholarlyArticle(
      context.database,
      convertToOrcidWorkId(proposedScholarlyArticle.sameAs),
      context.session,
    );

    if (knownArticle !== null) {
      return { result: knownArticle };
    }
  }

  try {
    const newScholarlyArticle: NewScholarlyArticle = {
      description: proposedScholarlyArticle.description,
      identifier: uuid(),
      name: proposedScholarlyArticle.name,
    };

    await service(
      context.database,
      context.session,
      newScholarlyArticle,
      periodicalIdentifier,
      sameAs,
      fullText,
    );

    return { result: newScholarlyArticle };
  } catch (e) {
    // tslint:disable-next-line:no-console
    console.log('Database error:', e);

    throw new Error('There was a problem creating a new article, please try again.');
  }
}
