import './styles.scss';

import * as React from 'react';
import { Redirect } from 'react-router';
import { RouteComponentProps } from 'react-router-dom';

import { ScholarlyArticle } from '../../../../../lib/interfaces/ScholarlyArticle';
import { Session } from '../../../../../lib/interfaces/Session';
import { getProfiles, getSession } from '../../services/account';
import { getProfile as getOrcidProfile, getWork as getOrcidWork } from '../../services/orcid';
import { getScholarlyArticle } from '../../services/periodical';
import { ArticleOverview } from '../articleOverview/component';
import { Spinner } from '../spinner/component';

interface ArticleOverviewPageState {
  article?: null | Partial<ScholarlyArticle>;
  session?: Session;
}
export interface ArticleOverviewPageRouteParams { articleId: string; }

export class ArticleOverviewPage
  extends React.Component<
  RouteComponentProps<ArticleOverviewPageRouteParams>,
  ArticleOverviewPageState
> {
  public state: ArticleOverviewPageState = {};

  constructor(props: any) {
    super(props);
  }

  public async componentDidMount() {
    try {
      const article = await this.fetchArticle(this.props.match.params.articleId);

      this.setState({ article });

      try {
        if (article && article.author && article.author.length > 0) {
          const ids = article.author
            .map((author) => author.identifier)
            // I'm using the type cast here to prevent having to write a user-defined type guard on the filter function:
            .filter((identifier) => typeof identifier !== 'undefined') as string[];

          if (ids.length > 0) {
            const profiles = await getProfiles(ids);

            this.setState((prevState) => ({
              article: {
                ...prevState.article,
                author: profiles,
              },
            }));
          }
        }
      } catch (e) {
        // Do nothing - we simply can't display profile info.
      }
    } catch (e) {
      this.setState({ article: null });
    }

    getSession()
    .then((session) => this.setState({ session }))
    .catch(() => {
      // Do nothing - we simply can't check whether the current user owns the current journal.
    });
  }

  public componentDidUpdate(_: any, prevState: ArticleOverviewPageState) {
    // We redirect to the PDF url when the query string is `?download`;
    // this allows us to link to our own domain for the download URL, which is required by Google Scholar.
    /* istanbul ignore next: Manipulating `window` doesn't work in tests */
    if (
      this.props &&
      this.props.location &&
      this.props.location.search &&
      this.props.location.search === '?download' &&
      !prevState.article &&
      this.state &&
      this.state.article &&
      this.state.article.associatedMedia &&
      this.state.article.associatedMedia[0] &&
      this.state.article.associatedMedia[0].contentUrl
    ) {
      window.location.replace(this.state.article.associatedMedia[0].contentUrl);
    }
  }

  public render() {
    return this.getView();
  }

  private getView() {
    if (
      this.state.article
      && this.state.article.identifier
      && this.state.article.identifier !== this.props.match.params.articleId
    ) {
      return <Redirect to={`/article/${this.state.article.identifier}`}/>;
    }
    if (typeof this.state.article === 'undefined') {
      return (
        <div className="container">
          <Spinner/>
        </div>
      );
    }
    if (this.state.article === null) {
      return (<div className="callout alert">This article could not be found.</div>);
    }

    return (
      <ArticleOverview
        article={this.state.article}
        session={this.state.session}
        slug={this.props.match.params.articleId}
        url={this.props.match.url}
      />
    );
  }

  private async fetchArticle(articleId: string): Promise<Partial<ScholarlyArticle> | null> {
    const article = await getScholarlyArticle(articleId);

    if (article.identifier) {
      return article;
    }

    if (/^\d{4}\-\d{4}\-\d{4}\-\d{3}(\d|X):\d+$/.test(articleId)) {
      const [ orcid, putCode ] = articleId.split(':');

      const [ [ person ], orcidArticle ] = await Promise.all([
        getOrcidProfile(orcid),
        getOrcidWork(orcid, putCode),
      ]);

      orcidArticle.author = [ person ];

      return orcidArticle;
    }

    return null;
  }
}
