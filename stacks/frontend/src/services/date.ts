import { DateTime } from 'luxon';

export function getFormattedDatetime(date: string) {
  const datetime = DateTime.fromISO(date);

  const format: Intl.DateTimeFormatOptions = {
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    month: 'long',
    year: 'numeric',
  };

  // Although we display the date in British English format (e.g. 5 February 2018),
  // Luxon offsets the actual date to the user's timezone:
  return datetime.setLocale('en-GB').toLocaleString(format);
}

export function getFormattedDate(date: string) {
  const datetime = DateTime.fromISO(date);

  const format: Intl.DateTimeFormatOptions = {
    day: 'numeric',
    month: 'long',
    year: 'numeric',
  };

  // Although we display the date in British English format (e.g. 5 February 2018),
  // Luxon offsets the actual date to the user's timezone:
  return datetime.setLocale('en-GB').toLocaleString(format);
}
